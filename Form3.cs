﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;


namespace loan2
{
    public partial class Form3 : Form
    {        

        System.Media.SoundPlayer player = new System.Media.SoundPlayer();

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        public Form3()
        {
            InitializeComponent();
            player.SoundLocation = "finalmusic.wav";
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result;

            result = MessageBox.Show("Are you sure, you want to clear all data?", "CLEAR!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (DialogResult.Yes == result)
            {
                comboBox1.Items.Clear();
                comboBox2.Items.Clear();
                comboBox3.Items.Clear();
                comboBox4.Items.Clear();
                comboBox5.Items.Clear();
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox1.Enabled = false;
                textBox2.Enabled = false;
                textBox3.Enabled = false;
                textBox4.Enabled = false;
                textBox5.Enabled = false;
                textBox6.Enabled = false;
                textBox7.Enabled = false;
                comboBox1.Items.Add("Honda");
                comboBox1.Items.Add("Suzuki");
                comboBox1.Items.Add("Toyota");
                comboBox1.Focus();
                label10.Visible = false;
                button1.Enabled = false;
                button2.Enabled = false;
                comboBox2.Enabled = false;
                comboBox3.Enabled = false;
                comboBox4.Enabled = false;
                comboBox5.Enabled = false;                
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label10.Visible = false;
            button2.Enabled = false;
            comboBox3.Enabled = false;
            comboBox4.Enabled = false;
            comboBox5.Enabled = false;
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            textBox5.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
            comboBox2.Items.Clear();
            comboBox3.Items.Clear();
            comboBox4.Items.Clear();
            comboBox5.Items.Clear();
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();

            if (comboBox1.Text == "Honda")
            {
                comboBox2.Items.Add("Civic");
                comboBox2.Items.Add("City");
                comboBox2.Items.Add("Accord");
            }

            if (comboBox1.Text == "Toyota")
            {
                comboBox2.Items.Add("Prado");
                comboBox2.Items.Add("Corolla");
                comboBox2.Items.Add("Vitz");
            }

            if (comboBox1.Text == "Suzuki")
            {
                comboBox2.Items.Add("Cultus");
                comboBox2.Items.Add("Mehran");
                comboBox2.Items.Add("Swift");
            }

            comboBox2.Enabled = true;
            button1.Enabled = true;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            label10.Visible = false;
            button2.Enabled = false;
            comboBox4.Enabled = false;
            comboBox5.Enabled = false;
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            textBox5.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
            comboBox3.Items.Clear();
            comboBox4.Items.Clear();
            comboBox5.Items.Clear();
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();


            if (comboBox2.Text == "Civic")
            {
                comboBox3.Items.Add("Reborn VVTi 1.5");
                comboBox3.Items.Add("Prosmatic VVTi 2.0");
            }

            if (comboBox2.Text == "City")
            {
                comboBox3.Items.Add("Aspire VVTi 1.5");
                comboBox3.Items.Add("Prosmatic VVTi 1.8");
            }

            if (comboBox2.Text == "Accord")
            {
                comboBox3.Items.Add("Manual VVTi 1.8");
                comboBox3.Items.Add("Hybrid VVTi 2.3");
            }

            if (comboBox2.Text == "Prado")
            {
                comboBox3.Items.Add("Manual VFX 1.5");
                comboBox3.Items.Add("Prosmatic VFX 1.8");
            }

            if (comboBox2.Text == "Corolla")
            {
                comboBox3.Items.Add("XLi VVTi 1.5");
                comboBox3.Items.Add("GLi VVTi 1.8");
            }

            if (comboBox2.Text == "Vitz")
            {
                comboBox3.Items.Add("Jewela CVT 1.3");
                comboBox3.Items.Add("Jewela CVT 1.5");
            }

            if (comboBox2.Text == "Cultus")
            {
                comboBox3.Items.Add("1st Gen VX 1.5");
                comboBox3.Items.Add("2nd Gen VX 1.5");
            }

            if (comboBox2.Text == "Swift")
            {
                comboBox3.Items.Add("Manual LXi 1.5");
                comboBox3.Items.Add("Prosmatic VXi 1.5");
            }

            if (comboBox2.Text == "Mehran")
            {
                comboBox3.Items.Add("VX 1.5");
                comboBox3.Items.Add("VX 1.5 CNG");
            }

            comboBox3.Enabled = true;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox4.Clear();
            comboBox4.Items.Clear();
            comboBox5.Items.Clear();
            label10.Visible = false;
            button2.Enabled = false;
            comboBox5.Enabled = false;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox5.Enabled = true;

            textBox1.Text = comboBox1.Text + " " + comboBox2.Text + " " + comboBox3.Text;
            if (comboBox3.Text == "Reborn VVTi 1.5")
                textBox2.Text = 2500000.ToString();
            if (comboBox3.Text == "Prosmatic VVTi 2.0")
                textBox2.Text = 3000000.ToString();
            if (comboBox3.Text == "Aspire VVTi 1.5")
                textBox2.Text = 1800000.ToString();
            if (comboBox3.Text == "Prosmatic VVTi 1.8")
                textBox2.Text = 2000000.ToString();
            if (comboBox3.Text == "Manual VVTi 1.8")
                textBox2.Text = 4000000.ToString();
            if (comboBox3.Text == "Hybrid VVTi 2.3")
                textBox2.Text = 4500000.ToString();
            if (comboBox3.Text == "Manual VFX 1.5")
                textBox2.Text = 6000000.ToString();
            if (comboBox3.Text == "Prosmatic VFX 1.8")
                textBox2.Text = 7500000.ToString();
            if (comboBox3.Text == "XLi VVTi 1.5")
                textBox2.Text = 1700000.ToString();
            if (comboBox3.Text == "GLi VVTi 1.8")
                textBox2.Text = 2100000.ToString();
            if (comboBox3.Text == "Jewela CVT 1.3")
                textBox2.Text = 900000.ToString();
            if (comboBox3.Text == "Jewela CVT 1.5")
                textBox2.Text = 1100000.ToString();
            if (comboBox3.Text == "1st Gen VX 1.5")
                textBox2.Text = 850000.ToString();
            if (comboBox3.Text == "2nd Gen VX 1.5")
                textBox2.Text = 900000.ToString();
            if (comboBox3.Text == "Manual LXi 1.5")
                textBox2.Text = 1050000.ToString();
            if (comboBox3.Text == "Prosmatic VXi 1.5")
                textBox2.Text = 1250000.ToString();
            if (comboBox3.Text == "VX 1.5")
                textBox2.Text = 700000.ToString();
            if (comboBox3.Text == "VX 1.5 CNG")
                textBox2.Text = 750000.ToString();

            comboBox4.Enabled = true;
            comboBox4.Items.Add("10%");
            comboBox4.Items.Add("20%");
            comboBox4.Items.Add("30%");
            comboBox4.Items.Add("40%");
            comboBox4.Items.Add("50%");
            comboBox4.Items.Add("60%");
            comboBox4.Items.Add("70%");
            comboBox4.Items.Add("80%");
            comboBox4.Items.Add("90%");
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            label10.Visible = false;
            button2.Enabled = false;
            textBox3.Enabled = true;
            textBox4.Enabled = true;
            textBox6.Enabled = true;
            textBox7.Enabled = true;
            comboBox5.Enabled = true;
            comboBox5.Items.Clear();

            long cprice = long.Parse(textBox2.Text);
            double dp = 0.0;
            double ramount;

            if (comboBox4.Text == "10%")
            {
                dp = (10 * cprice) / 100;
            }
            if (comboBox4.Text == "20%")
            {
                dp = (20 * cprice) / 100;
            }
            if (comboBox4.Text == "30%")
            {
                dp = (30 * cprice) / 100;
            }
            if (comboBox4.Text == "40%")
            {
                dp = (40 * cprice) / 100;
            }
            if (comboBox4.Text == "50%")
            {
                dp = (50 * cprice) / 100;
            }
            if (comboBox4.Text == "60%")
            {
                dp = (60 * cprice) / 100;
            }
            if (comboBox4.Text == "70%")
            {
                dp = (70 * cprice) / 100;
            }
            if (comboBox4.Text == "80%")
            {
                dp = (80 * cprice) / 100;
            }
            if (comboBox4.Text == "90%")
            {
                dp = (90 * cprice) / 100;
            }

            ramount = cprice - dp;

            textBox3.Text = dp.ToString();
            textBox4.Text = ramount.ToString();

            comboBox5.Items.Add("1 Year");
            comboBox5.Items.Add("2 Years");
            comboBox5.Items.Add("3 Years");
            comboBox5.Items.Add("4 Years");
            comboBox5.Items.Add("5 Years");
            comboBox5.Items.Add("6 Years");
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            label10.Visible = true;
            button2.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult result;

            result = MessageBox.Show("Are you sure, you want to leave?", "EXIT!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (DialogResult.Yes == result)
            {
                Application.Exit();
            }
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboBox2.Focus();
            }
        }

        private void comboBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboBox3.Focus();
            }
        }

        private void comboBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboBox4.Focus();
            }
        }

        private void comboBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboBox5.Focus();
            }
        }

        private void comboBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button2.Focus();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            long tamount = long.Parse(textBox2.Text);
            long ramount = long.Parse(textBox4.Text);
            int months = 0;

            if (comboBox5.Text == "1 Year")
                months = 12;
            if (comboBox5.Text == "2 Years")
                months = 24;
            if (comboBox5.Text == "3 Years")
                months = 36;
            if (comboBox5.Text == "4 Years")
                months = 48;
            if (comboBox5.Text == "5 Years")
                months = 60;
            if (comboBox5.Text == "6 Years")
                months = 72;

            double HBL_MI = (ramount / months) + (0.35 * tamount) / 100;
            double AB_MI  = (ramount / months) + (0.39 * tamount) / 100;
            double UBL_MI = (ramount / months) + (0.375 * tamount) / 100;
            double SC_MI  =  (ramount / months) + (0.41 * tamount) / 100;
            double SB_MI  =  (ramount / months) + (0.32 * tamount) / 100;
            double MB_MI  =  (ramount / months) + (0.31 * tamount) / 100;

            labelhb.Text  = "Rs: " + HBL_MI;
            labelab.Text = "Rs: " + AB_MI;
            labelubl.Text = "Rs: " + UBL_MI;
            labelsc.Text  = "Rs: " + SC_MI;
            labelsb.Text  = "Rs: " + SB_MI;
            labelmb.Text  = "Rs: " + MB_MI;


            if (comboBox2.Text == "Civic")
               { panel4.Visible = true; }

            if (comboBox2.Text == "Accord")
               { panel4.Visible = true; panel5.Visible = true; }

            if (comboBox2.Text == "City")
               { panel4.Visible = true; panel5.Visible = true; panel6.Visible = true; }

            if (comboBox2.Text == "Prado")
               {
                 panel4.Visible = true; panel5.Visible = true; panel6.Visible = true;
                 panel7.Visible = true;
               }

            if (comboBox2.Text == "Corolla")
               {
                 panel4.Visible = true; panel5.Visible = true; panel6.Visible = true;
                 panel7.Visible = true; panel8.Visible = true;
               }

            if (comboBox2.Text == "Vitz")
               {
                 panel4.Visible = true; panel5.Visible = true; panel6.Visible = true;
                 panel7.Visible = true; panel8.Visible = true; panel9.Visible = true;
               }

            if (comboBox2.Text == "Swift")
               {
                 panel4.Visible = true; panel5.Visible = true; panel6.Visible = true;
                 panel7.Visible = true; panel8.Visible = true; panel9.Visible = true;
                 panel10.Visible = true;
               }

            if (comboBox2.Text == "Cultus")
               {
                 panel4.Visible = true; panel5.Visible = true; panel6.Visible = true;
                 panel7.Visible = true; panel8.Visible = true; panel9.Visible = true;
                 panel10.Visible = true; panel11.Visible = true;
               }


            if (comboBox2.Text == "Mehran")
               {
                 panel4.Visible = true; panel5.Visible = true; panel6.Visible = true;
                 panel7.Visible = true; panel8.Visible = true; panel9.Visible = true;
                 panel10.Visible = true; panel11.Visible = true; panel12.Visible = true;
               }

           
            label10.Visible = false;
            panel2.Visible = true;
            panel3.Visible = false;
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button5.Visible = true;
            player.Play();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            panel4.Visible = false;
            panel5.Visible = false;
            panel6.Visible = false;
            panel7.Visible = false;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = false;
            panel11.Visible = false;
            panel12.Visible = false;
            button5.Visible = false;
            panel3.Visible = true;
            button6.Visible = true;
            checkBox1.Visible = true;
            checkBox2.Visible = true;
            checkBox3.Visible = true;
            checkBox4.Visible = true;
            checkBox5.Visible = true;
            checkBox6.Visible = true;
            player.Stop();

        }


        private void button6_Click(object sender, EventArgs e)
        {
            label10.Visible = true;
            panel2.Visible = false;
            panel3.Visible = false;
            button6.Visible = false;
            button7.Enabled = false;
            button1.Visible = true;
            button2.Visible = true;
            button3.Visible = true;
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox6.Checked = false;
            checkBox1.Visible = false;
            checkBox2.Visible = false;
            checkBox3.Visible = false;
            checkBox4.Visible = false;
            checkBox5.Visible = false;
            checkBox6.Visible = false;
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult result;

            result = MessageBox.Show("Are you sure, you want to leave?", "EXIT!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (DialogResult.Yes == result)
            {
                Application.Exit();
            }
        }

        private void panel4_VisibleChanged(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (pictureBox7.Visible == true)
            {
                pictureBox7.Visible = false;
                pictureBox8.Visible = true;
            }

            else if (pictureBox8.Visible == true)
            {
                pictureBox8.Visible = false;
                pictureBox9.Visible = true;
            }

            else if (pictureBox9.Visible == true)
            {
                pictureBox9.Visible = false;
                pictureBox10.Visible = true;
            }

            else if (pictureBox10.Visible == true)
            {
                pictureBox10.Visible = false;
                pictureBox11.Visible = true;
            }

            else if (pictureBox11.Visible == true)
            {
                pictureBox11.Visible = false;
                pictureBox12.Visible = true;
            }

            else if (pictureBox12.Visible == true)
            {
                pictureBox12.Visible = false;
                pictureBox13.Visible = true;
            }

            else if (pictureBox13.Visible == true)
            {
                pictureBox13.Visible = false;
                pictureBox7.Visible = true;
            }
        
        }

        private void panel5_VisibleChanged(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

            if (pictureBox14.Visible == true)
            {
                pictureBox14.Visible = false;
                pictureBox15.Visible = true;
            }

            else if (pictureBox15.Visible == true)
            {
                pictureBox15.Visible = false;
                pictureBox16.Visible = true;
            }

            else if (pictureBox16.Visible == true)
            {
                pictureBox16.Visible = false;
                pictureBox17.Visible = true;
            }

            else if (pictureBox17.Visible == true)
            {
                pictureBox17.Visible = false;
                pictureBox18.Visible = true;
            }

            else if (pictureBox18.Visible == true)
            {
                pictureBox18.Visible = false;
                pictureBox19.Visible = true;
            }

            else if (pictureBox19.Visible == true)
            {
                pictureBox19.Visible = false;
                pictureBox14.Visible = true;
            }
        
        }

        private void panel6_VisibleChanged(object sender, EventArgs e)
        {
            timer3.Start();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (pictureBox20.Visible == true)
            {
                pictureBox20.Visible = false;
                pictureBox21.Visible = true;
            }

            else if (pictureBox21.Visible == true)
            {
                pictureBox21.Visible = false;
                pictureBox22.Visible = true;
            }

            else if (pictureBox22.Visible == true)
            {
                pictureBox22.Visible = false;
                pictureBox23.Visible = true;
            }

            else if (pictureBox23.Visible == true)
            {
                pictureBox23.Visible = false;
                pictureBox24.Visible = true;
            }

            else if (pictureBox24.Visible == true)
            {
                pictureBox24.Visible = false;
                pictureBox25.Visible = true;
            }

            else if (pictureBox25.Visible == true)
            {
                pictureBox25.Visible = false;
                pictureBox26.Visible = true;
            }

            else if (pictureBox26.Visible == true)
            {
                pictureBox26.Visible = false;
                pictureBox20.Visible = true;
            }

        }

        private void panel7_VisibleChanged(object sender, EventArgs e)
        {
            timer4.Start();
        }

        private void timer4_Tick(object sender, EventArgs e)
        {

            if (pictureBox27.Visible == true)
            {
                pictureBox27.Visible = false;
                pictureBox28.Visible = true;
            }

            else if (pictureBox28.Visible == true)
            {
                pictureBox28.Visible = false;
                pictureBox29.Visible = true;
            }

            else if (pictureBox29.Visible == true)
            {
                pictureBox29.Visible = false;
                pictureBox30.Visible = true;
            }

            else if (pictureBox30.Visible == true)
            {
                pictureBox30.Visible = false;
                pictureBox31.Visible = true;
            }

            else if (pictureBox31.Visible == true)
            {
                pictureBox31.Visible = false;
                pictureBox32.Visible = true;
            }

            else if (pictureBox32.Visible == true)
            {
                pictureBox32.Visible = false;
                pictureBox33.Visible = true;
            }

            else if (pictureBox33.Visible == true)
            {
                pictureBox33.Visible = false;
                pictureBox27.Visible = true;
            }

        }

        private void panel8_VisibleChanged(object sender, EventArgs e)
        {
            timer5.Start();
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            if (pictureBox34.Visible == true)
            {
                pictureBox34.Visible = false;
                pictureBox35.Visible = true;
            }

            else if (pictureBox35.Visible == true)
            {
                pictureBox35.Visible = false;
                pictureBox36.Visible = true;
            }

            else if (pictureBox36.Visible == true)
            {
                pictureBox36.Visible = false;
                pictureBox37.Visible = true;
            }

            else if (pictureBox37.Visible == true)
            {
                pictureBox37.Visible = false;
                pictureBox38.Visible = true;
            }

            else if (pictureBox38.Visible == true)
            {
                pictureBox38.Visible = false;
                pictureBox39.Visible = true;
            }

            else if (pictureBox39.Visible == true)
            {
                pictureBox39.Visible = false;
                pictureBox40.Visible = true;
            }

            else if (pictureBox40.Visible == true)
            {
                pictureBox40.Visible = false;
                pictureBox34.Visible = true;
            }
        }

        private void panel9_VisibleChanged(object sender, EventArgs e)
        {
            timer6.Start();
        }

        private void timer6_Tick(object sender, EventArgs e)
        {
            if (pictureBox41.Visible == true)
            {
                pictureBox41.Visible = false;
                pictureBox42.Visible = true;
            }

            else if (pictureBox42.Visible == true)
            {
                pictureBox42.Visible = false;
                pictureBox43.Visible = true;
            }

            else if (pictureBox43.Visible == true)
            {
                pictureBox43.Visible = false;
                pictureBox44.Visible = true;
            }

            else if (pictureBox44.Visible == true)
            {
                pictureBox44.Visible = false;
                pictureBox45.Visible = true;
            }

            else if (pictureBox45.Visible == true)
            {
                pictureBox45.Visible = false;
                pictureBox46.Visible = true;
            }

            else if (pictureBox46.Visible == true)
            {
                pictureBox46.Visible = false;
                pictureBox47.Visible = true;
            }

            else if (pictureBox47.Visible == true)
            {
                pictureBox47.Visible = false;
                pictureBox41.Visible = true;
            }
        }

        private void panel10_VisibleChanged(object sender, EventArgs e)
        {
            timer7.Start();
        }

        private void timer7_Tick(object sender, EventArgs e)
        {
            if (pictureBox48.Visible == true)
            {
                pictureBox48.Visible = false;
                pictureBox49.Visible = true;
            }

            else if (pictureBox49.Visible == true)
            {
                pictureBox49.Visible = false;
                pictureBox50.Visible = true;
            }

            else if (pictureBox50.Visible == true)
            {
                pictureBox50.Visible = false;
                pictureBox51.Visible = true;
            }

            else if (pictureBox51.Visible == true)
            {
                pictureBox51.Visible = false;
                pictureBox52.Visible = true;
            }

            else if (pictureBox52.Visible == true)
            {
                pictureBox52.Visible = false;
                pictureBox48.Visible = true;
            }
        }

        private void panel11_VisibleChanged(object sender, EventArgs e)
        {
            timer8.Start();
        }

        private void timer8_Tick(object sender, EventArgs e)
        {

            if (pictureBox53.Visible == true)
            {
                pictureBox53.Visible = false;
                pictureBox54.Visible = true;
            }

            else if (pictureBox54.Visible == true)
            {
                pictureBox54.Visible = false;
                pictureBox55.Visible = true;
            }

            else if (pictureBox55.Visible == true)
            {
                pictureBox55.Visible = false;
                pictureBox56.Visible = true;
            }

            else if (pictureBox56.Visible == true)
            {
                pictureBox56.Visible = false;
                pictureBox57.Visible = true;
            }

            else if (pictureBox57.Visible == true)
            {
                pictureBox57.Visible = false;
                pictureBox53.Visible = true;
            }

        }

        private void panel12_VisibleChanged(object sender, EventArgs e)
        {
            timer9.Start();
        }

        private void timer9_Tick(object sender, EventArgs e)
        {
            if (pictureBox58.Visible == true)
            {
                pictureBox58.Visible = false;
                pictureBox59.Visible = true;
            }

            else if (pictureBox59.Visible == true)
            {
                pictureBox59.Visible = false;
                pictureBox60.Visible = true;
            }

            else if (pictureBox60.Visible == true)
            {
                pictureBox60.Visible = false;
                pictureBox61.Visible = true;
            }

            else if (pictureBox61.Visible == true)
            {
                pictureBox61.Visible = false;
                pictureBox62.Visible = true;
            }

            else if (pictureBox62.Visible == true)
            {
                pictureBox62.Visible = false;
                pictureBox58.Visible = true;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                button7.Enabled = true;
            }
            else
            {
                checkBox1.Checked = false;
                button7.Enabled = false;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                checkBox1.Checked = false;
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                button7.Enabled = true;
            }
            else
            {
                checkBox2.Checked = false;
                button7.Enabled = false;
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                button7.Enabled = true;
            }
            else
            {
                checkBox3.Checked = false;
                button7.Enabled = false;
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked == true)
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                button7.Enabled = true;
            }
            else
            {
                checkBox4.Checked = false;
                button7.Enabled = false;
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked == true)
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                checkBox6.Checked = false;
                button7.Enabled = true;
            }
            else
            {
                checkBox5.Checked = false;
                button7.Enabled = false;
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked == true)
            {
                checkBox1.Checked = false;
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                button7.Enabled = true;
            }
            else
            {
                checkBox6.Checked = false;
                button7.Enabled = false;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Now you will be directed to Official Site of Selected Bank for further Process.. Go Head?", "ASK!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (checkBox1.Checked == true)
                { System.Diagnostics.Process.Start("http://www.hbl.com/carloan"); }

                if (checkBox2.Checked == true)
                { System.Diagnostics.Process.Start("http://www.soneribank.com/retail-banking/consumer-finance/soneri-car-finance/"); }

                if (checkBox3.Checked == true)
                { System.Diagnostics.Process.Start("https://www.meezanbank.com/car-ijarah/"); }

                if (checkBox4.Checked == true)
                { System.Diagnostics.Process.Start("https://www.sc.com/ae/islamic/saadiq-auto-finance.html"); }

                if (checkBox5.Checked == true)
                { System.Diagnostics.Process.Start("http://www.ubldirect.com/Corporate/BankingServices/ConsumerLoans/UBLDrive/Home.aspx"); }

                if (checkBox6.Checked == true)
                { System.Diagnostics.Process.Start("https://www.ally.com/auto/"); }
            }
        }
    }
}
